# Hitchy type definitions

## License

[MIT](LICENSE)

## About

This package is extracting definition of types specific to the [Hitchy server-side framework](https://core.hitchy.org/) to make them available independently of the framework's core improving their use with plugins which usually do not have the core as a dependency (but peer dependency).

## Usage

Install with

```bash
npm i -D @hitchy/types
```

