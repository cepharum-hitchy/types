import {
    IncomingMessage as HttpIncomingMessage,
    ServerResponse as HttpServerResponse,
    Server as HttpServer
} from "http";
import {EventEmitter} from "events";
import {URL} from "url";

declare namespace Hitchy.Core {
    /**
     * Describes instance of Hitchy suitable for injecting into some server.
     */
    export interface Instance {
        /**
         * Exposes name of injector used to create this instance of Hitchy.
         */
        injector: string;

        /**
         * Promises Hitchy instance being ready for handling requests.
         */
        onStarted: Promise<API>;

        /**
         * Provides callback for shutting down Hitchy instance.
         */
        stop(): Promise<any>;

        /**
         * Exposes instance of Hitchy API as used by this Hitchy instance.
         */
        api: API;

        /**
         * Exposes instance of Hitchy API as used by this Hitchy instance.
         */
        hitchy: API;
    }

    /**
     * Describes instance of Hitchy suitable for injecting into Node.js HTTP server.
     */
    export interface NodeInstance extends Instance {
    }

    /**
     * Describes instance of Hitchy suitable for injecting into Connect/Express
     * application as middleware.
     */
    export interface ConnectInstance extends Instance {
    }

    /**
     * Provides all Hitchy-related information available in modules and functions of
     * a Hitchy-based application.
     */
    export interface API extends LibraryAPI {
        /**
         * Exposes application's current runtime configuration.
         */
        config: Config;

        /**
         * Exposes application's meta information read from files package.json
         * and/or hitchy.json in application's root folder.
         */
        meta: object;

        /**
         * Exposes available components grouped by type of component.
         *
         * @deprecated
         */
        runtime: Runtime;

        /**
         * Maps available plugins' roles into either plugin's API.
         */
        plugins: Plugins;

        /**
         * Exposes data storage available for globally saving volatile data during
         * application runtime exceeding lifetime of single requests.
         */
        data: object;

        /**
         * Exposes all controller components of application.
         */
        controllers: ControllerComponents;

        /**
         * Exposes all controller components of application.
         */
        controller: ControllerComponents;

        /**
         * Exposes all policy components of application.
         */
        policies: PolicyComponents;

        /**
         * Exposes all policy components of application.
         */
        policy: PolicyComponents;

        /**
         * Exposes all model components of application.
         */
        models: ModelComponents;

        /**
         * Exposes all model components of application.
         */
        model: ModelComponents;

        /**
         * Exposes all service components of application.
         */
        services: ServiceComponents;

        /**
         * Exposes all service components of application.
         */
        service: ServiceComponents;

        /**
         * Exposes server-side websocket API when @hitchy/plugin-socket.io has
         * been installed.
         */
        websocket: {
            io: EventEmitter;
        }

        /**
         * Intentionally crashes Hitchy-based service.
         *
         * @param cause cause of crash
         * @return promises Hitchy instance and request listener being torn down
         */
        crash(cause: Error): Promise<any>;

        /**
         * Intentionally shuts down Hitchy-based service.
         *
         * @return promises Hitchy instance and request listener being torn down
         */
        shutdown(): Promise<any>;
    }

    /**
     * Provides elements of Hitchy's API distributed as part of its core and thus
     * instantly available at start of bootstrap.
     */
    export interface LibraryAPI extends EventEmitter {
        /**
         * Generates logger functions.
         *
         * @param facility name of logging facility
         */
        log: LoggerGenerator;

        router: RouterAPI;

        responder: ResponderAPI;

        utility: UtilityAPI;

        /**
         * Conveniently simplifies loading module with optional support for common
         * module pattern.
         *
         * @param modulePath path name of module to load
         * @param moduleArguments arguments to pass in addition to module complying with common module pattern
         */
        loader: (modulePath: string, moduleArguments?: any[]) => (object | Function | Promise<object | Function>);

        cmp: (modulePath: string, customArgs?: any[]) => any;

        cmfp: (fn: CMP, customArgs?: any[]) => any;

        Client: RouterClient;

        /**
         * Resolves provided folder in relation to current project's folder.
         *
         * The provided name may start with `@hitchy/` to be resolved in relation to
         * folder containing currently used hitchy instance.
         *
         * @param folder name to be resolved
         */
        folder: (folder: string) => string;
    }

    /**
     * Describes function implementing module in compliance with Hitchy's
     * "common module pattern".
     *
     * @see https://core.hitchy.org/internals/patterns.html#common-module-pattern
     */
    export type CMP<T = any> = (this: API, options: Options, ...customArgs: any[]) => T;

    export interface RouterAPI {
        dispatch(context: RequestContext): Promise<RequestContext>;

        client: RouterClient;
    }

    export interface ResponderAPI {
        normalize(context: RequestContext): RequestContext;
    }

    export interface UtilityAPI {
        object: {
            /**
             * Deeply seals provided data object.
             *
             * @param data object to be sealed deeply
             * @param testFn callback invoked with segments of current properties breadcrumb for deciding if property shall be frozen or not
             */
            seal(data: object, testFn?: (segments: string[]) => boolean): object,

            /**
             * Deeply freezes provided data object.
             *
             * @param data object to be frozen deeply
             * @param testFn callback invoked with segments of current properties breadcrumb for deciding if property shall be frozen or not
             */
            freeze(data: object, testFn?: (segments: string[]) => boolean): object,

            /**
             * Deeply merges provided source objects into given target object
             * returning the latter.
             *
             * @param target target object adjusted by merging
             * @param source single source object or list of source objects to be deeply merged into given target
             * @param strategyFn callback invoked to decide strategy for merging particular property
             */
            merge(target: object, source: (object | object[]), strategyFn: (breadcrumb: string[], strategy: MergeStrategy, sourceValue: any, targetValue: any) => MergeStrategy): object,
        },

        case: {
            /**
             * Converts naming style of provided string from kebab-case to camelCase.
             *
             * @param input string to be converted
             */
            kebabToCamel(input: string): string,

            /**
             * Converts naming style of provided string from kebab-case to PascalCase.
             *
             * @param input string to be converted
             */
            kebabToPascal(input: string): string,

            /**
             * Converts naming style of provided string from camelCase to kebab-case.
             *
             * @param input string to be converted
             */
            camelToKebab(input: string): string,

            /**
             * Converts naming style of provided string from PascalCase to kebab-case.
             *
             * @param input string to be converted
             */
            pascalToKebab(input: string): string,

            /**
             * Converts naming style of provided string from camelCase to PascalCase.
             *
             * @param input string to be converted
             */
            camelToPascal(input: string): string,
        },

        value: {
            /**
             * Detects boolean keyword in provided string returning related boolean
             * value or provided string if detection fails.
             *
             * @param input string to be inspected
             * @returns boolean value detected in provided string
             */
            asBoolean(input: string): boolean | string,
        },

        logger: LoggerGenerator,
    }

    /**
     * Selects a strategy of merging a particular property when deeply merging to
     * objects.
     */
    export enum MergeStrategy {
        /**
         * Demands to skip merging current property.
         */
        KeepSource = "keep",

        /**
         * Demands to deeply merge value of source property into related target
         * property.
         */
        DeepMerge = "merge",

        /**
         * Demands to replace whole property of target with property provided by
         * source.
         */
        Replace = "replace",

        /**
         * Demands to create/extend list of values in target property with value(s)
         * found in source property.
         */
        Concat = "concat",
    }

    /**
     * Describes API of client for internally dispatching requests.
     */
    export interface RouterClient {
        new(options: RouterClientOptions): RouterClient;

        dispatch: () => Promise<ServerResponse>;
        url: string;
        method: string;
        rawHeaders: string[];
        headers: { [key: string]: string };
        rawTrailers: string[];
        trailers: { [key: string]: string };
        socket: {
            address: object;
            localAddress: string;
            localPort: number;
            remoteAddress: string;
            remotePort: number;
            remoteFamily: string;
        };
        response: ServerResponse;
        httpVersion: string;
    }

    /**
     * Describes request to be dispatched internally.
     */
    export interface RouterClientOptions {
        /**
         * Provides path name and query of request.
         */
        url: string;

        /**
         * Provides HTTP method of request.
         */
        method: string;

        /**
         * Lists custom request headers.
         */
        headers: { [key: string]: string };
    }

    /**
     * Represents options provided on starting Hitchy-based application e.g. via
     * CLI parameters.
     */
    export interface Options {
        /**
         * Selects folder containing application to be presented.
         */
        projectFolder: string;

        /**
         * Selects root folder of current instance of Hitchy framework.
         */
        hitchyFolder: string;

        /**
         * Selects separate folder containing node_modules/ subfolder w/ Hitchy
         * plugins to be discovered. Omit to use same folder as `projectFolder`.
         */
        pluginsFolder?: string;

        /**
         * Set true to enable very noisy debugging by enabling any existing
         * logging facility.
         */
        debug?: boolean;

        /**
         * If true, Hitchy is handling errors rather than passing them back into
         * service it is injecting application into (e.g. Express).
         */
        handleErrors?: boolean;

        /**
         * Lists folders of plugins to be loaded explicitly.
         */
        explicitPlugins?: String[];

        /**
         * Set true for loading explicitly provided plugin folders, only.
         */
        explicitPluginsOnly?: boolean;

        /**
         * Lists dependencies to enable for current project.
         *
         * This replaces list in project's hitchy.json file or the default behaviour
         * of enabling all available plugins.
         */
        dependencies: string[];

        /**
         * Lists all arguments provided on CLI.
         */
        arguments: object;

        /**
         * Provides custom logging manager to use instead of Hitchy's default.
         * Some capturing logger is injected by unit testing framework appending
         * all messages in @see TestContext.logged when `true` is provided here.
         */
        logger?: ( true | AbstractLogger );
    }

    /**
     * Describes basic API of every logger manager that can be injected to
     * replace Hitchy's default logger manager.
     */
    export abstract class AbstractLogger {
        /**
         * Fetches logging function for selected namespace.
         *
         * @param namespace namespace of all messages logged by invoking returned function
         */
        get( namespace: string ): LoggerFunction;

        /**
         * Updates logging functions actually generating log records or not
         * based on provided switches.
         *
         * @param switches comma-separated list of namespace patterns controlling matching namespaces to be enabled or disabled
         */
        update( switches: string ): void;
    }

    /**
     * Maps available plugins' roles into either plugin's API.
     */
    export interface Plugins {
        [key: string]: PluginAPI;
    }

    /**
     * Maps available plugins' name into either plugin's handle.
     */
    export interface PluginHandles {
        [key: string]: PluginHandle;
    }

    /**
     * Describes common elements of some plugin's API.
     */
    export interface PluginAPI {
        /**
         * Exposes name of plugin.
         */
        $name: string;

        /**
         * Exposes role of plugin.
         */
        $role: string;

        /**
         * Exposes meta information on plugin.
         */
        $meta: object;

        /**
         * Exposes plugin's particular configuration loaded from Javascript files in
         * its **config** sub-folder.
         */
        $config: object;

        /**
         * Gets invoked when all plugins' APIs have been loaded.
         *
         * @param options options used on invoking application
         * @param plugins all available plugins' handles
         * @param handle current plugin's handle
         */
        onDiscovered?(this: API, options: Options, plugins: PluginHandles, handle: PluginHandle): void;

        /**
         * Gets invoked before loading and exposing components of plugin.
         *
         * @param options options used on invoking application
         * @param handle current plugin's handle
         */
        onExposing?(this: API, options: Options, handle: PluginHandle): void;

        /**
         * Gets invoked after having loaded and exposed components of plugin.
         *
         * @param options options used on invoking application
         * @param handle current plugin's handle
         */
        onExposed?(this: API, options: Options, handle: PluginHandle): void;

        /**
         * Gets invoked after global configuration has been compiled from either
         * plugin's as well as application's particular configuration.
         *
         * @param options options used on invoking application
         * @param handle current plugin's handle
         */
        configure?(this: API, options: Options, handle: PluginHandle): void;

        /**
         * Gets invoked after having exposed all components of every available
         * plugin as well as application itself.
         *
         * This callback is meant to initialise either plugin's resources if
         * required.
         *
         * @param options options used on invoking application
         * @param handle current plugin's handle
         */
        initialize?(this: API, options: Options, handle: PluginHandle): void;

        /**
         * Provides plugin's routing declarations of policies.
         */
        policies?: PluginRoutingDeclaration;

        /**
         * Provides plugin's routing declarations of terminal routes.
         */
        routes?: PluginRoutingDeclaration;

        /**
         * Provides plugin's routing declarations of blueprint routes.
         */
        blueprints?: PluginRoutingDeclaration;

        /**
         * Gets invoked on gracefully shutting down Hitchy-based application.
         *
         * This callback is meant to release either plugin's resources if required.
         *
         * @param options options used on invoking application
         * @param handle current plugin's handle
         */
        shutdown?(this: API, options: Options, handle: PluginHandle): void;
    }

    /**
     * Describes single plugin during bootstrap of Hitchy-based application.
     */
    export interface PluginHandle {
        /**
         * Provides name of plugin.
         */
        name: string;

        /**
         * Caches role claimed by plugin in its **hitchy.json** file.
         */
        staticRole: string;

        /**
         * Provides folder containing plugin.
         */
        folder: string;

        /**
         * Provides particular configuration of plugin loaded from Javascript files
         * found in its **config** sub-folder.
         */
        config: object;

        /**
         * Exposes meta information on plugin read from its **hitchy.json** file as
         * well as probably extended by its API.
         */
        meta: object;

        /**
         * Provides API exported by plugin.
         */
        api: PluginAPI;
    }

    /**
     * Fetches logger function for selected namespace.
     */
    export type LoggerGenerator = (namespace: string) => LoggerFunction;

    /**
     * Logs provided message with optionally provided arguments injected in
     * context of a namespace.
     */
    export type LoggerFunction = (message: string, ...args: any) => void;

    /**
     * Represents essential subset of either Hitchy-based application's
     * configuration.
     */
    export interface Config {
        /**
         * Exposes part of global configuration that originate's from current
         * application's configuration files, only.
         */
        $appConfig: Config;

        /**
         * Exposes routing declarations for (terminal) routes.
         */
        routes: RoutingConfig;

        /**
         * Exposes routing declarations for policies.
         */
        policies: RoutingConfig;

        /**
         * Exposes routing declarations for blueprints.
         */
        blueprints: RoutingConfig;

        /**
         * Exposes configuration affecting behaviour of Hitchy's core.
         */
        hitchy: CoreConfig;
    }

    /**
     * Describes part of configuration affecting behaviour of Hitchy's core.
     */
    export interface CoreConfig {
        /**
         * Controls whether names of folders containing components per type of
         * component are reversed and appended to either resulting component's name
         * or not.
         *
         * This parameter defaults to `true` and must be set `false` explicitly to
         * have folders' names prepended without reversing order.
         *
         * @note This parameter works per plugin or related to the application, only.
         */
        appendFolders?: boolean;

        /**
         * Controls whether Hitchy is searching for component modules in sub-folders
         * per type of components.
         *
         * This parameter defaults to `true` and must be set `false` explicitly to
         * ignore and sub-folders per type of component.
         *
         * @note This parameter works per plugin or related to the application, only.
         */
        deepComponents?: boolean;
    }

    export type RoutingConfig =
        RoutingDeclaration
        | RoutingSlotDeclaration;

    /**
     * Describes routing declarations grouped by routing slot for explicit
     * separation.
     */
    export interface RoutingSlotDeclaration {
        early?: RoutingDeclaration;
        before?: RoutingDeclaration;
        after?: RoutingDeclaration;
        late?: RoutingDeclaration;
    }

    /**
     * Lists routing sources mapped into routing targets.
     */
    export interface RoutingDeclaration {
        [key: string]: RoutingTargetDeclaration;
    }

    export type RoutingDeclarationProvider = (this: API, options: Options, handle: PluginHandle) => (RoutingDeclaration | Promise<RoutingDeclaration>);
    export type PluginRoutingDeclaration =
        RoutingDeclarationProvider
        | RoutingDeclaration
        | Promise<RoutingDeclaration>;

    export type RoutingTargetDeclaration =
        RoutingTargetFunction
        | RoutingTargetName
        | RoutingTargetDescriptor;
    export type RoutingTargetFunction =
        RequestControllerHandler
        | RequestPolicyHandler;
    export type RoutingTargetName = String;

    export interface RoutingTargetDescriptor {
        module: ControllerComponent | PolicyComponent;
        method: RequestControllerHandler | RequestPolicyHandler;
        args?: any[];
    }

    /**
     * Defines signature of a controller's request handler.
     */
    export type RequestControllerHandler = (this: RequestContext, req: IncomingMessage, res: ServerResponse, ...customParameters: any[]) => Promise<any>;

    /**
     * Defines signature of a policy's request handler.
     */
    export type RequestPolicyHandler = (this: RequestContext, req: IncomingMessage, res: ServerResponse, next: ContinuationHandler, ...customParameters: any[]) => Promise<any>;

    /**
     * Defines signature of callbacks to be invoked for passing request handling to
     * next handler in chain.
     */
    export type ContinuationHandler = (error?: Error) => void;

    /**
     * Defines collections of components grouped by type of component.
     */
    export interface Runtime {
        controllers: ControllerComponents;
        controller: ControllerComponents;
        policies: PolicyComponents;
        policy: PolicyComponents;
        models: ModelComponents;
        model: ModelComponents;
        services: ServiceComponents;
        service: ServiceComponents;
    }

    export interface ControllerComponents {
        [key: string]: ControllerComponent;
    }

    export interface PolicyComponents {
        [key: string]: PolicyComponent;
    }

    export interface ModelComponents {
        [key: string]: ModelComponent;
    }

    export interface ServiceComponents {
        [key: string]: ServiceComponent;
    }

    export interface Component {
    }

    export interface ControllerComponent extends Component {
    }

    export interface PolicyComponent extends Component {
    }

    export interface ModelComponent extends Component {
    }

    export interface ServiceComponent extends Component {
    }

    /**
     * Implements custom exception captured by Hitchy to respond with particular
     * HTTP status.
     */
    export class HttpException extends Error implements ServiceComponent {
        /**
         * @param httpStatusCode HTTP status code to return
         * @param errorMessage message to include with HTTP response
         */
        constructor( httpStatusCode: number, errorMessage: string );

        /**
         * Exposes HTTP status code as provided on constructing the exception.
         */
        statusCode: number;
    }

    /**
     * Extends IncomingMessage of Node.js in context of a Hitchy-based application.
     */
    export interface IncomingMessage extends HttpIncomingMessage {
        hitchy: API;
        api: API;
        context: RequestContext;
        accept: string[];
        cookies?: { [key: string]: string };
        params: { [key: string]: string | string[] };
        path: string;
        query: { [key: string]: string | string[] };
        session?: { [key: string]: any };

        fetchBody(parser?: boolean | BodyParser): Promise<any>;

        /**
         * Demands to omit any follow-up handler matching current request.
         */
        ignore(): void;
    }

    /**
     * Extends ServerResponse of Node.js in context of a Hitchy-based application.
     */
    export interface ServerResponse extends HttpServerResponse {
        /**
         * Sends response data ending response afterwards.
         *
         * @param content response body
         * @param encoding optional encoding of response body used when providing content as string
         */
        send(content: (string | Buffer), encoding?: string): ServerResponse;

        /**
         * Adjusts HTTP response status code.
         *
         * @param statusCode HTTP status code, e.g. 200 for OK
         */
        status(statusCode: number): ServerResponse;

        /**
         * Adjusts response header indicating type of content in response body.
         *
         * @param typeName string selecting type of content, should be MIME identifier, but may be some short alias as well
         */
        type(typeName: string): ServerResponse;

        /**
         * Picks one of the provided callbacks according to request's Accept header
         * for generating the actual response.
         *
         * @param handlers set of handlers, each generating response in a different format
         */
        format(handlers: { [key: string]: RequestControllerHandler }): ServerResponse;

        /**
         * Generates JSON-formatted response from provided set of data.
         *
         * @param data data to be sent in response
         */
        json(data: object): ServerResponse;

        /**
         * Adjusts response header.
         *
         * @param headersOrName name of header to adjust or object containing multiple header fields to adjust
         * @param value value of single named header field to adjust
         */
        set(headersOrName: ({ [key: string]: string } | string), value: string): ServerResponse;

        /**
         * Adjusts response headers to demand redirection.
         *
         * @param statusCode redirection status code to use, omit to use default 302
         * @param url redirection URL client is asked to fetch instead
         */
        redirect(statusCode: number, url: string): ServerResponse;
    }

    /**
     * Implements parser for request body.
     */
    export type BodyParser = (raw: Buffer) => (any | Promise<any>);

    /**
     * Describes context via `this` in request handlers of a Hitchy-based
     * application.
     */
    export interface RequestContext {
        request: IncomingMessage;
        response: ServerResponse;

        /**
         * Some request-bound data storage suitable for sharing data between
         * handlers of same request.
         */
        local: { [key: string]: any };

        api: API;
        config: Config;
        runtime: Runtime;
        controllers: ControllerComponents;
        controller: ControllerComponents;
        policies: PolicyComponents;
        policy: PolicyComponents;
        models: ModelComponents;
        model: ModelComponents;
        services: ServiceComponents;
        service: ServiceComponents;
        startTime: number;
        context: ContextIdentifier;
        done: ContinuationHandler;
        /** @private */
        consumed: boolean;
        /** Provides absolute URL of current request as used by client most probably. */
        url: URL;
        /** Exposes IP address of current request's client. */
        clientIp: string;

        /** If true, no more request handlers are invoked to handle the request. */
        ignored: boolean;
    }

    export enum ContextIdentifier {
        Standalone = "standalone",
        Express = "express",
    }

    export type TestBoundClient = (url: string, body: Buffer | string | object, headers: object) => Promise<ServerResponse>;
    export type TestUnboundClient = (method: string, url: string, body: Buffer | string | object, headers: object) => Promise<ServerResponse>;

    export interface TestContext {
        hitchy: Instance;
        server: HttpServer;
        logged?: string[];
        get: TestBoundClient;
        post: TestBoundClient;
        put: TestBoundClient;
        patch: TestBoundClient;
        delete: TestBoundClient;
        head: TestBoundClient;
        options: TestBoundClient;
        trace: TestBoundClient;
        request: TestUnboundClient;
    }
}
