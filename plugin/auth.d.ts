import * as Ref from "../plugin/odem";

/**
 * Declares types used with Hitchy's auth plugin.
 */
declare namespace Hitchy.Plugin.Auth {
    export interface User extends Ref.Hitchy.Plugin.Odem.Model {
    }

    export interface SamlConfig {
        entryPoint: string;
        issuer: string;
        signatureAlgorithm: string;
        callbackUrl: string;
        logoutCallbackUrl: string;
        cert: string;
    }

    export interface OidcConfig {
        discoveryUrl: string;
        clientId: string;
        clientSecret: string;
        redirectUrl: string;
        callbackUrl: string;
        responseTypes: string;
        postLogoutRedirectUris: string;
        customConfig?: { [key: string]: any };
    }
}
